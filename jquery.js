"use strict";

var imgnumber = 1;
var clickvar = 0;


$(document).ready(function (){
    $("#left-arrow").click(function (){
        switch (imgnumber){
            case 1:
                imgnumber = 6;
                $("#mapimg").attr("src","img/longestyard.jpg");
                $("#map-name").text("Q3DM17: The Longest Yard - Quake 3: Arena");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                // console.log("---");
                // console.log($("#map-name").text());
                // console.log("---");
                break;

            case 2:
                imgnumber = 1;
                $("#mapimg").attr("src","img/Dust-2.jpg");
                $("#map-name").text("Dust 2 - Counter Strike series");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;
            case 3:
                imgnumber = 2;
                $("#mapimg").attr("src","img/facing-worlds.jpg");
                $("#map-name").text("Facing Worlds - Unreal Tournament series");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;
            case 4:
                imgnumber = 3;
                $("#mapimg").attr("src","img/blood_gulch.jpg");
                $("#map-name").text("Blood Gulch - Halo: Combat Evolved");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;
            case 5:
                imgnumber = 4;
                $("#mapimg").attr("src","img/Terminal_mw2.jpg");
                $("#map-name").text("Terminal - Call of Duty: Modern Warfare series");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;

            case 6:
                imgnumber = 5
                $("#mapimg").attr("src","img/pubg.jpg");
                $("#map-name").text("Erangel - PUBG");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;
        }
    });

    $("#right-arrow").click(function (){
        switch (imgnumber){
            case 1:
                imgnumber = 2;
                $("#mapimg").attr("src","img/facing-worlds.jpg");
                $("#map-name").text("Facing Worlds - Unreal Tournament series");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;

            case 2:
                imgnumber = 3;
                $("#mapimg").attr("src","img/blood_gulch.jpg");
                $("#map-name").text("Blood Gulch - Halo: Combat Evolved");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;
            case 3:
                imgnumber = 4;
                $("#mapimg").attr("src","img/Terminal_mw2.jpg");
                $("#map-name").text("Terminal - Call of Duty: Modern Warfare series");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;
            case 4:
                imgnumber = 5;
                $("#mapimg").attr("src","img/pubg.jpg");
                $("#map-name").text("Erangel - PUBG");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;
            case 5:
                imgnumber = 6;
                $("#mapimg").attr("src","img/longestyard.jpg");
                $("#map-name").text("Q3DM17: The Longest Yard - Quake 3: Arena");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
                break;

            case 6:
                imgnumber = 1;
                $("#mapimg").attr("src","img/Dust-2.jpg");
                $("#map-name").text("Dust 2 - Counter Strike series");
                $("#pageNumber").text(`${imgnumber}/6`);
                if (clickvar == 1){
                    $('#map-desc').removeClass("showmap");
                    $('#map-desc').addClass("hidemap");
                    clickvar = 0;
                }
        }
    });

    $("#map-name").click(function (){
        switch (imgnumber){
            case 1:
                $("#map-desc").text("de_dust2 is a demolition map from the original Counter Strike that has been in every version of the game since. It is by far the most popular Counter Strike map and for some the only map. The best thing to do on T side is rush B. Nobody would expect the enemy team to rush B 15 times in a row.");
                // console.log("switch case 1");
                break;
            case 2:
                $("#map-desc").text("Facing worlds is a CTF map from Unreal Tournament that has been remade in later series, an in UT2k4 included alongside its remake version and was a map in the demo of UT2k4.");
                break;
            case 3:
                $('#map-desc').text("Blood Gulch is a multiplayer map from Halo: Combat evolved that while only appearing in Halo CE, heavily inspired a map in each of the later games: Halo 2 - Coagulation, Halo 3 - Vallhalla, Halo: Reach - Hemorrhage, Halo 4 - Ragnarok. It was also the map from the demo of the first version of Halo CE on the PC and the primary setting of Red vs Blue. It also saved the Scorpion from being scrapped due to not being featured in the campaign enough because it was fun to drive in Blood Gulch. The PC version features Banshees on top of the bases and replaces one Warthog per side with a Rocket Hog");
                break;
            case 5:
                $('#map-desc').text("Erangel was the first and originally the only map in PLAYERUNKNOWN'S BATTLEGROUNDS. It is an island with a Soviet theme ");
                break;
            case 4:
                $('#map-desc').text("Terminal is a multiplayer map from Modern Warfare 2 that was also added in as free DLC to Modern Warfare 3. It is also featured in Call of Duty Mobile and was initially only pre order bonus for Infinite Warfare but is free to everyone as of December 2016");
                break;
            case 6:
                $('#map-desc').text("The Longest Yard is a deathmatch map from Quake 3: Arena. It features many jump pads, little cover and no OSHA compliance so deaths by falling or being pushed off the map are common. Also a good map to hone your railgun skills");
                break;
        }
        if(clickvar == 0){
            $('#map-desc').addClass("showmap");
            $('#map-desc').removeClass("hidemap");
            clickvar = 1;
        } else {
            $('#map-desc').removeClass("showmap");
            $('#map-desc').addClass("hidemap");
            clickvar = 0;
        }

    });





    // $("#map-name").hover(
    //     function (){
    //         console.log("before the switch");
    //         switch (imgnumber){
    //             case 1:
    //                 $("#map-desc").text("de_dust2 is a demolition map from the original Counter Strike that has been in every version of the game since. It is by far the most popular Counter Strike map and for some the only map. The best thing to do on T side is rush B. Nobody would expect the enemy team to rush B 15 times in a row.");
    //                 // console.log("switch case 1");
    //                 break;
    //             case 2:
    //                 $("#map-desc").text("Facing worlds is a CTF map from Unreal Tournament that has been remade in later series, an in UT2k4 included alongside its remake version and was a map in the demo of UT2k4.");
    //                 break;
    //             case 3:
    //                 $('#map-desc').text("Blood Gulch is a multiplayer map from Halo: Combat evolved that while only appearing in Halo CE, heavily inspired a map in each of the later games: Halo 2 - Coagulation, Halo 3 - Vallhalla, Halo: Reach - Hemorrhage, Halo 4 - Ragnarok. It was also the map from the demo of the first version of Halo CE on the PC and the primary setting of Red vs Blue. It also saved the Scorpion from being scrapped due to not being featured in the campaign enough because it was fun to drive in Blood Gulch. The PC version features Banshees on top of the bases and replaces one Warthog per side with a Rocket Hog");
    //                 break;
    //             case 5:
    //                 $('#map-desc').text("Erangel was the first and originally the only map in PLAYERUNKNOWN'S BATTLEGROUNDS. It is an island with a Soviet theme ");
    //                 break;
    //             case 4:
    //                 $('#map-desc').text("Terminal is a multiplayer map from Modern Warfare 2 that was also added in as free DLC to Modern Warfare 3. It is also featured in Call of Duty Mobile and was initially only pre order bonus for Infinite Warfare but is free to everyone as of December 2016");
    //                 break;
    //             case 6:
    //                 $('#map-desc').text("The Longest Yard is a deathmatch map from Quake 3: Arena. It features many jump pads, little cover and no OSHA compliance so deaths by falling or being pushed off the map are common. Also a good map to hone your railgun skills");
    //                 break;
    //         }
    //         console.log("after the switch")
    //         $('#map-desc').removeClass('hidemap');
    //         $("#map-desc").addClass("showmap");
    //         console.log(document.getElementById("map-desc").classList);
    //     },
    //
    //     function(){
    //         console.log("2nd section function of hover")
    //         $('#map-desc').addClass('hidemap');
    //         $("#map-desc").removeClass("showmap");
    //         console.log(document.getElementById("map-desc").classList);
    //
    //     }
    //
    // );
});

